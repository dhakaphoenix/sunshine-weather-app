package com.example.root.sunshineapp;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    ArrayAdapter<String> mForecastAdapter=null;
    public MainActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater) {
        // Inflate the menu; this adds items to the action bar if it is present.
        inflater.inflate(R.menu.forecast_refresh, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if(id == R.id.action_refresh)
        {
            FetchWeathertask weatherTask = new FetchWeathertask();
            weatherTask.execute("110011");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        final String[] forecastArray = {

                "today - sunny - 88/63",
                "tommorow - foggy - 84/74",
                "wednesday - astroids - 89/79",
                "thursday - rainy - 70/56",
                "friday - foggy - 60/56",
                "saturday - rainy - 70/55",
                "sunday - cloudy - 65/59"
        };
        List<String> weekForecast = new ArrayList<String>(Arrays.asList(forecastArray));
        mForecastAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.list_item_forecast,
                R.id.list_view_text, weekForecast);
        ListView listview = (ListView) rootView.findViewById(R.id.list_view);
        listview.setAdapter(mForecastAdapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String forecast = mForecastAdapter.getItem(position);
                Intent intent = new Intent(getActivity(),DetailActivity.class).putExtra(Intent.EXTRA_TEXT,forecast);
                startActivity(intent);
            }
        });
        return rootView;
    }



    class FetchWeathertask extends AsyncTask<String,Void,String[]>
        {

            @Override
            protected String[] doInBackground(String... params) {

                if(params.length == 0)
                    return null;

                String format = "json";
                String units = "metric";
                int numOfDays = 7;

                final String FORECAST_BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?";
                final String QUERY_PARAM = "q";
                final String FORMAT_PARAM = "mode";
                final String UNITS_PARAM = "units";
                final String DAYS_PARAM = "cnt";


                HttpURLConnection urlConnection  = null;
                BufferedReader reader = null;
                String forecastJsonString = null;


                try {
                    Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()
                            .appendQueryParameter(QUERY_PARAM, params[0])
                            .appendQueryParameter(FORMAT_PARAM,format)
                            .appendQueryParameter(UNITS_PARAM,units)
                            .appendQueryParameter(DAYS_PARAM, Integer.toString(numOfDays)).build();

                    URL url = new URL(builtUri.toString());
                    Log.v("url building",url.toString());

                    urlConnection = (HttpURLConnection)url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.connect();

                    InputStream inputStream = urlConnection.getInputStream();
                    StringBuffer buffer = new StringBuffer();
                    if(inputStream == null)
                    {
                        return null;
                    }

                    reader= new BufferedReader(new InputStreamReader(inputStream));
                    String line=null;

                    while((line = reader.readLine()) != null)
                    {
                        buffer.append(line+ "\n");
                    }

                    if(buffer.length() == 0)
                    {return  null;
                    }
                    forecastJsonString=buffer.toString();
                    Log.v("JSON REPLY",forecastJsonString);

                } catch (MalformedURLException e) {
                    Log.e("OnCreateView","Url error",e);
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    Log.e("onCreateView","protocol error",e);
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.e("onCreateView","error getting data from server",e);
                    e.printStackTrace();
                }
                finally {
                    if(urlConnection != null)
                        urlConnection.disconnect();

                    if(reader != null)
                        try {
                            reader.close();
                        } catch (IOException e) {
                            Log.e("onCreateView","error closing reader",e);
                            e.printStackTrace();
                        }
                }
                try {
                    return getWeatherDataFromJson(forecastJsonString,numOfDays);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return  null;
            }

            @Override
            protected void onPostExecute(String[] result) {
                if(result != null)
                    mForecastAdapter.clear();
                for(String dayForecastResult:result)
                    mForecastAdapter.add(dayForecastResult);
            }
        }
             private String[] getWeatherDataFromJson(String forecastJsonString,int numOfDays) throws JSONException {
                 final String OWM_LIST = "list";
                 final String OWM_WEATHER = "weather";
                 final String OWM_TEMPERATURE = "temp";
                 final String OWM_MAX = "max";
                 final String OWM_MIN = "min";
                 final String OWM_DESCRIPTION =  "main";

                 JSONObject forecastJson = new JSONObject(forecastJsonString);
                 JSONArray weatherArray = forecastJson.getJSONArray(OWM_LIST);
                 android.text.format.Time dayTime = new android.text.format.Time();
                 dayTime.setToNow();

                 int julianStartDay = android.text.format.Time.getJulianDay(System.currentTimeMillis(),dayTime.gmtoff);

                 dayTime = new android.text.format.Time();

                 String[] resultStrs = new String[numOfDays];

                 for(int i=0;i<weatherArray.length();i++)
                 {
                     String day = null;
                     String description;
                     String highAndLow;
                     JSONObject dayForecast = weatherArray.getJSONObject(i);

                     long dateTime;
                     dateTime = dayTime.setJulianDay(julianStartDay+i);
                     Date date = new Date();
                     int temp = date.getDay()+i;
                     if(temp>6)
                        temp=temp-7;
                     switch (temp)
                     {
                         case 0: day = "Sunday";
                           break;
                         case 1: day =  "Monday";
                             break;
                         case 2: day = "Tuesday";
                             break;
                         case 3: day = "Wednesday";
                             break;
                         case 4: day = "Thursday";
                             break;
                         case 5: day = "Friday";
                             break;
                         case 6: day = "Saturday";
                             break;
                         default:
                             break;

                    }

                     JSONObject weatherObject = dayForecast.getJSONArray(OWM_WEATHER).getJSONObject(0);
                     description = weatherObject.getString(OWM_DESCRIPTION);

                     JSONObject tempObject = dayForecast.getJSONObject(OWM_TEMPERATURE);
                     Double max = tempObject.getDouble(OWM_MAX);
                     Double min = tempObject.getDouble(OWM_MIN);
                     highAndLow = ""+max+"/"+min;
                     resultStrs[i]=day+"-"+description+"-"+highAndLow;
                 }
                 for(String s:resultStrs)
                   Log.v("weather string",s);
                 return resultStrs;

             }

}
